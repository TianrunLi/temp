#!/usr/bin/env python
# encoding: utf-8

import time
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

import mnist

class Mnist(object):
    """Docstring for Mnist. """
    def __init__(self):
        super(Mnist, self).__init__()
        self.IMAGE_SIZE = 28 * 28
        self.NUM_CLASS = 10
        self.BATCH_SIZE = 100
        self.learning_rate = 1e-4
        self.conv_params = [32, 64]
        self.hidden_params = [1024, 128]

        self.kernel = mnist.MNIST_kernel()
        self.dataset = input_data.read_data_sets('MNIST_data/', one_hot=True)

    def run(self):
        input_x = tf.placeholder(tf.float32, [None, self.IMAGE_SIZE])
        input_y = tf.placeholder(tf.float32, [None, self.NUM_CLASS])
        keep_rate = tf.placeholder(tf.float32)

        # MNIST_kernel builds all the dataflow graph and implementations
        # here, you just record all the Tensors you want to evaluate
        output = self.kernel.build_forward(keep_rate, input_x, self.conv_params, self.hidden_params)
        loss = self.kernel.build_loss(input_y, output)
        optimizer = self.kernel.build_train(loss, self.learning_rate)
        accuracy = self.kernel.build_evaluate(input_y, output)
        summary = tf.summary.merge_all()

        with tf.Session() as sess:
            #summary_writer = tf.summary.FileWriter('log', sess.graph)
            #summary_writer.flush()
            sess.run(tf.global_variables_initializer())
            for i in range(1000):
                batch_x, batch_y = self.dataset.train.next_batch(self.BATCH_SIZE)
                sess.run(optimizer, {input_x: batch_x,
                                     input_y: batch_y,
                                     keep_rate: 0.5})
                if i % 50 == 0:
                    tune_accuracy, summary_string = sess.run(
                                            [accuracy, summary],
                                            {   input_x: self.dataset.validation.images,
                                                input_y: self.dataset.validation.labels,
                                                keep_rate: 1.0})
                    print ('tune set accuracy is %f' % (tune_accuracy))
                    #summary_writer.add_summary(summary_string, i)
                    #summary_writer.flush()
            test_batch_x, test_batch_y = self.dataset.test.next_batch(self.BATCH_SIZE)
            test_accuracy = sess.run(accuracy, {input_x: test_batch_x,
                                                input_y: test_batch_y,
                                                keep_rate: 1.0})
            print ('test set accuracy is %f' % (test_accuracy))

def main():
    mnist = Mnist()
    mnist.run()

if __name__ == "__main__":
    main()
