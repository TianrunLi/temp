#!/usr/bin/env python
# encoding: utf-8

import tensorflow as tf

class MNIST_kernel(object):
    """docstring for MNIST_kernel"""
    def __init__(self):
        super(MNIST_kernel, self).__init__()
        # dataset parameter
        self.IMAGE_LENGTH = 28
        self.IMAGE_SIZE = 28 * 28
        self.NUM_CLASS = 10

        # network parameter
        self.CONV_SIZE = 5
        self.POOL_SIZE = 2

    def conv2d(self, x, W):
        return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

    def pooling(self, x):
        return tf.nn.max_pool(x, ksize=[1,self.POOL_SIZE,self.POOL_SIZE,1],
                                strides=[1,self.POOL_SIZE,self.POOL_SIZE,1], padding='SAME')

    def gen_weight(self, shape, name):
        initial = tf.truncated_normal(shape, stddev=0.1)
        return tf.Variable(initial, tf.float32, name=name)

    def gen_bias(self, shape, name):
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial, tf.float32, name=name)

    def gen_conv_kernel(self, input_channel, output_channel):
        return self.gen_weight([self.CONV_SIZE, self.CONV_SIZE,
                                input_channel, output_channel],
                               'kernel')

    def build_forward(self, keep_rate, input_batch, conv_params, hidden_params):
        current = tf.reshape(input_batch, [-1, self.IMAGE_LENGTH, self.IMAGE_LENGTH, 1])
        current_size = self.IMAGE_SIZE
        for it in range(len(conv_params)):
            with tf.name_scope('conv' + repr(it)):
                input_channel = 1 if it == 0 else conv_params[it-1]
                conv_kernel = self.gen_conv_kernel(input_channel, conv_params[it])
                conv_bias = self.gen_bias([conv_params[it]], 'bias')
                current_size = current_size / input_channel * conv_params[it]

                current = self.pooling(tf.nn.relu(self.conv2d(current, conv_kernel) + conv_bias))
                current_size = current_size / self.POOL_SIZE / self.POOL_SIZE

        with tf.name_scope('norm'):
            current_size = int(current_size)
            current = tf.reshape(current, [-1, current_size])

        for it in range(len(hidden_params)):
            with tf.name_scope('fc' + repr(it)):
                weight = self.gen_weight([current_size, hidden_params[it]], 'weight')
                bias = self.gen_bias([hidden_params[it]], 'bias')

                current = tf.nn.relu(tf.matmul(current, weight) + bias)
                current = tf.nn.dropout(current, keep_rate)
                current_size = hidden_params[it]

        with tf.name_scope('output'):
            weight = self.gen_weight([current_size, self.NUM_CLASS], 'weight')
            bias = self.gen_bias([self.NUM_CLASS], 'bias')
            current = tf.matmul(current, weight) + bias
            # add a histogram summary for final weight
            tf.summary.histogram('final weight', weight)
        return current

    def build_loss(self, true_label, output):
        with tf.name_scope('loss'):
            cross_entrypy_error = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(labels=true_label, logits=output),
                name='error')
            # add a scalar summary for error
            tf.summary.scalar('cross entropy error', cross_entrypy_error)
            return cross_entrypy_error

    def build_train(self, loss, learning_rate):
        with tf.name_scope('optimizer'):
            optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
            return optimizer

    def build_evaluate(self, true_label, output):
        with tf.name_scope('evaluate'):
            correct = tf.equal(tf.argmax(true_label, 1),
                                tf.argmax(output, 1))
            return tf.reduce_mean(tf.cast(correct, tf.float32))

def main():
    kernel = MNIST_kernel()
    return kernel.gen_weight([1,2,3,4], 'weight')

if __name__ == "__main__":
    main()
